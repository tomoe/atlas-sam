import logging
import itertools
import random
import urlparse
import re
import json

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

CE_METRICS = (
    'org.sam.CONDOR-JobSubmit-/atlas/Role=lcgadmin',
)

WEBDAV_METRICS = (
'webdav.HTTP-DIR_HEAD-/atlas/Role=production',
'webdav.HTTP-DIR_GET-/atlas/Role=production',
'webdav.HTTP-FILE_PUT-/atlas/Role=production',
'webdav.HTTP-FILE_GET-/atlas/Role=production',
'webdav.HTTP-FILE_OPTIONS-/atlas/Role=production',
'webdav.HTTP-FILE_MOVE-/atlas/Role=production',
'webdav.HTTP-FILE_HEAD-/atlas/Role=production',
'webdav.HTTP-FILE_HEAD_ON_NON_EXISTENT-/atlas/Role=production',
'webdav.HTTP-FILE_PROPFIND-/atlas/Role=production',
'webdav.HTTP-FILE_DELETE-/atlas/Role=production',
'webdav.HTTP-FILE_DELETE_ON_NON_EXISTENT-/atlas/Role=production',
'webdav.HTTP-TLS_CIPHERS-/atlas/Role=production'
)

METHODS = ('Get', 'Put', 'Del')
TOKENS = ('', 'DATA', 'GROUP', 'LOCALGROUP', 'SCRATCH')

SRM_METRICS = ['org.atlas.DDM-srm-%s%s-/atlas/Role=production' % (m, t)
               for (m, t) in itertools.product(METHODS, TOKENS)]
SRM_METRICS.extend(['org.atlas.DDM-srm-LsDir-/atlas/Role=production'])

GSI_METRICS = ['org.atlas.DDM-gsiftp-%s%s-/atlas/Role=production' % (m, t)
               for (m, t) in itertools.product(METHODS, TOKENS)]
GSI_METRICS.extend(['org.atlas.DDM-gsiftp-LsDir-/atlas/Role=production'])

ROOT_METRICS = ['org.atlas.DDM-root-%s%s-/atlas/Role=production' % (m, t)
                for (m, t) in itertools.product(METHODS, TOKENS)]
ROOT_METRICS.extend(['org.atlas.DDM-root-LsDir-/atlas/Role=production'])

HTTPS_METRICS = ['org.atlas.DDM-https-%s%s-/atlas/Role=production' % (m, t)
                 for (m, t) in itertools.product(METHODS, TOKENS)]
HTTPS_METRICS.extend(['org.atlas.DDM-https-LsDir-/atlas/Role=production'])

FLAVOR_MAP = {'CREAM-CE': 'cream',
              'ARC-CE': 'arc',
              'HTCONDOR-CE': 'condor',
              'GLOBUS': 'gt',
              'OSG-CE': 'gt'}

WN_METRICS = {
    'CE-ATLAS-sft-vo-cvmfs': 'org.atlas.WN-cvmfs-/atlas/Role=lcgadmin',
    'CE-ATLAS-sft-singularity-Frontier-Squid': 'org.atlas.WN-FrontierSquid-/atlas/Role=lcgadmin',
    'CE-ATLAS-sft-vo-swspace': 'org.atlas.WN-swspace-/atlas/Role=lcgadmin',
    'CE-ATLAS-sft-singularity': 'org.atlas.WN-singularity-/atlas/Role=lcgadmin',
}
WN_LEGACY_METRICS = {
    'CE-ATLAS-sft-singularity-Frontier-Squid': 'org.atlas.WN-FrontierSquid-/atlas/Role=lcgadmin',
    'CE-ATLAS-sft-vo-swspace': 'org.atlas.WN-swspace-/atlas/Role=lcgadmin',
    'CE-ATLAS-sft-singularity': 'org.atlas.WN-singularity-/atlas/Role=lcgadmin',
}

SAME_CODES = {'OK': 0, 'INFO': 3, 'NOTICE': 3, 'WARNING': 1, 'ERROR': 2, 'CRITICAL': 2, 'MAINTENANCE': 3}


def run(url, cert, key):
    log.info("Processing vo feed: %s" % url)

    # Get services from the VO feed, i.e
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url, cert=cert, key=key)
    services = feed.get_services()

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/x_plugins_vofeed_atlas_hosts_generated.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add host groups
    sites = feed.get_groups("ATLAS_Site")
    hg = Groups("host_groups")
    for site, hosts in sites.items():
        for host in hosts:
            hg.add(site, host)
    hg.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/x_plugins_vofeed_atlas_checks_generated.cfg
    c = Checks()
    c.add_all(CE_METRICS,     tags=["ARC-CE", "HTCONDOR-CE"])
    c.add_all(WN_METRICS.values(), tags=["ARC-CE", "HTCONDOR-CE"])
    c.add_all(SRM_METRICS,    tags=["SRMv2", "OSG-SRMv2", "SRM"])
    c.add_all(GSI_METRICS,    tags=["GRIDFTP"])
    c.add_all(ROOT_METRICS,   tags=["XROOTD"])
    c.add_all(HTTPS_METRICS,  tags=["WEBDAV"])
    c.add_all(WEBDAV_METRICS, tags=["WEBDAV",])

    # WEBDAV setup
    for service in services:
        host = service[0]
        flavor = service[1]
        endpoint = service[2]
        if flavor not in ["WEBDAV", ]:
            continue
        se_resources = feed.get_se_resources(host, flavor)
        datadisk = [ se[1] for se in se_resources if 'DATADISK' in se[0] ]
        if not datadisk:
            log.warning("Datadisk path missing for service %s (%s)" % (host, flavor))
            continue
        if len(datadisk) > 1:
            log.warning("Multiple datadisks detected for service %s (%s)" % (host, flavor))
        datadisksamdir = re.sub(r'/rucio/$', '/SAM/', datadisk[0])
        if not endpoint:
            c.add('webdav.HTTP-All-/atlas/Role=production', hosts=(host,), params={'args': {'--uri': "https://"+host+datadisksamdir}, '_unique_tag': ' WEBDAV'})
        else:
            uri = urlparse.urljoin(endpoint.replace("davs:", "https:"), datadisksamdir)
            c.add('webdav.HTTP-All-/atlas/Role=production', hosts=(host,), params={'args': {'--uri': uri}, '_unique_tag': 'WEBDAV'})

    # ETF env - environment variables to export on the worker node (global for all sites), such as:
    # ETF_TESTS - points to a list of WN tests to execute (stored in WN_METRICS)
    # ETF_LEGACY should be a subset of ETF_TESTS that identifies SFT tests (those that are not nagios compliant)
    # SAME* environment variables are needed by the legacy/SFT tests
    with open('/tmp/etf-env.sh', 'w') as etf_env:
        etf_env.write('ETF_TESTS={}\n'.format(','.join(['etf/probes/org.atlas/'+m for m in WN_METRICS.keys()])))
        etf_env.write('ETF_LEGACY={}\n'.format(','.join(['etf/probes/org.atlas/' + m for m in WN_LEGACY_METRICS.keys()])))
        for code, value in SAME_CODES.items():
            etf_env.write('SAME_{}={}\n'.format(code, value))
        etf_env.write('SAME_VO=atlas\n')
        etf_env.write('SAME_TEST_DIRNAME=etf/probes/org.atlas\n')
        etf_env.write('SAME_SENSOR_HOME=etf/probes/org.atlas\n')

    # ETF WN-qFM config - maps WN tests to metrics (WN-cvmfs -> org.lhcb.WN-cvmfs-/lhcb/Role=production)
    with open('/tmp/etf_wnfm.json', 'w') as etf_wnfm:
        json.dump({'wn_metric_map': WN_METRICS, 'counter_enabled': True}, etf_wnfm)

    # Queue selection algorithm for job submission
    for service in services:
        host = service[0]
        flavor = service[1]
        site = hg.exact_match(host)
        if flavor not in ["ARC-CE", "OSG-CE", "HTCONDOR-CE", "GLOBUS"]:
            continue
        site = hg.exact_match(host)
        ce_resources = feed.get_ce_resources(host, flavor)
        if service[0] == 'ce-grid.grid.uaic.ro':
            c.add('org.sam.CONDOR-JobState-/atlas/Role=lcgadmin', ('ce-grid.grid.uaic.ro',),
                                    params={'args': {'--resource': 'cream://ce-grid.grid.uaic.ro/nosched/pbs/atlas'}})
        elif ce_resources:
            ce_res_default = [e for e in ce_resources if e[2] == 'true']
            # picks random queues if no queue marked as ETF default
            # or more than one queue marked as ETF default
            if not ce_res_default:
                # no ETF default queue
                log.warning("No ETF default queue for service %s (%s)" % (host, flavor))
                ce_res = random.choice(ce_resources)
                batch = ce_res[0] or 'nopbs'
                queue = ce_res[1]
            elif len(ce_res_default) > 1:
                # more than one ETF default queue
                log.warning("More than one ETF default queue specified for service %s (%s)" % (host, flavor))
                ce_res = random.choice(ce_res_default)
                batch = ce_res[0] or 'nopbs'
                queue = ce_res[1]
            else:
                batch = ce_res_default[0][0] or 'nopbs'
                queue = ce_res_default[0][1]
            if flavor not in FLAVOR_MAP.keys():
                log.warning("Unable to determine type for flavour %s" % flavor)
                continue
            res = "%s://%s/%s/%s/%s" % (FLAVOR_MAP[flavor], host, host, batch, queue)
            # also adding environment variable (SITE) to be exported on the WN
            if 'nikhef' in host:
                c.add('org.sam.ARC-JobState-/atlas/Role=lcgadmin', hosts=(host,),
                      params={'args': {'--resource': '%s' % res, '--arc-debug': 'VERBOSE', }})
                c.add('org.sam.ARC-JobSubmit-/atlas/Role=lcgadmin', hosts=(host,))
            c.add('org.sam.CONDOR-JobState-/atlas/Role=lcgadmin', (host,),
                  params={'args': {'--resource': '%s' % res, '--env': 'SITE=\"{}\"'.format(site.pop()), 
                                   '--jdl-ads': '\"+maxWallTime=10\"'}})
        else:
            log.warning("No ce_resources found for host %s, BDII will used" % host)

    c.serialize()
