#!/usr/bin/env python

import socket
import requests
import argparse
try:
    import json
except ImportError:
    import simplejson as json

workdir = '/var/lib/gridprobes/atlas.Role=production/org.atlas/DDM/'
tested_tokens = [ 'atlasdatadisk', 'atlasgroupdisk', 'atlaslocalgroupdisk', 'atlasscratchdisk', 't2atlasdatadisk', 't2atlasgroupdisk', 't2atlaslocalgroupdisk', 't2atlasscratchdisk' ]
cric_ddm = "https://atlas-cric.cern.ch/api/atlas/ddmendpoint/query/?json"
cert     = "/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem";
key      = "/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem";
srm_cric_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_srm_storage_list_tmp.json','srm_provcric.json')['lxplus' in socket.gethostname()]
xrd_cric_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_root_storage_list_tmp.json','root_provcric.json')['lxplus' in socket.gethostname()]
htp_cric_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_https_storage_list_tmp.json','https_provcric.json')['lxplus' in socket.gethostname()]
gsi_cric_file = ('/usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_gsiftp_storage_list_tmp.json','gsiftp_provcric.json')['lxplus' in socket.gethostname()]

class cacher(object):

    def getCRICATLASInfo(self):

        rest_cric = requests.get(cric_ddm, verify=False, timeout=120, cert=(cert, key))
        if rest_cric.status_code != 200:
            print ("Failed to retrieve endpooints at %s" % cric_ddm)

        ddm_json = json.loads(rest_cric.text)
        ddm_qrt = []
        for name,el in ddm_json.iteritems():
           if el['se'] != None:
              ddm_qrt.append((el['name'],el['se']+el['endpoint'],el['token'],el['rc_site']))
           else:
              ddm_qrt.append((el['name'],''+el['endpoint'],el['token'],el['rc_site']))
        #la riga sopra e quella sotto sono passibili di migliore organizzazione?
#        ddms = filter(lambda en: en[1][-5:] == 'disk/',
#                      list(set( [ ((em[0],em[1],em[2],em[3]),(em[0].split('_')[0]+'_GROUPDISK',em[1][0:em[1].rfind('atlasgroupdisk/')+15],em[2],em[3]))['atlasgroupdisk' in em[1] ]
#                                  for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[0].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower())
#                                                                                          for ep in tested_tokens]), ddm_qrt) ] )))
#        ddms = filter(lambda en: en[1].split('/')[-2] in tested_tokens or en[1].split('/')[-3] in tested_tokens or en[2].lower() in tested_tokens,

#        ddms = filter(lambda en: en[2].lower() in tested_tokens,
#                      list(set( [ ((em[0],em[1],em[2],em[3]),(em[0].split('_')[0]+'_GROUPDISK',em[1][0:em[1].rfind('atlasgroupdisk/')+15],em[2],em[3]))['atlasgroupdisk' in em[1]]
#                                  for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[2].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower())
#                                                                                          for ep in tested_tokens]), ddm_qrt) ] )))

        #if 'atlas/atlas' in i[1]&& and&& i[2].lower() not in i[1]
        #flt_ddm=filter(lambda it: it[2].lower() in it[1], [em for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[2].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower()) for ep in tested_tokens]), ddm_qrt)])
        flt_ddm=[em for em in filter(lambda el: reduce(lambda x,y: x or y, [ ((ep.lower() in el[2].lower() or ep.lower() in el[1].lower()) and 'pps' not in el[0].lower()) for ep in tested_tokens]), ddm_qrt)]
        ddms = filter(lambda en: en[2].lower() in tested_tokens,
                      list(set( [ ((em[0],em[1],em[2],em[3]),(em[0].split('_')[0]+'_GROUPDISK',em[1][0:em[1].rfind('atlasgroupdisk/')+15],em[2],em[3]))['atlasgroupdisk' in em[1]]
                                  for em in flt_ddm ] )))

        srm_cric_dict = {}
        xrd_cric_dict = {}
        htp_cric_dict = {}
        gsi_cric_dict = {}
        for ddm in ddms:
            perm_list = []
            #ddm_attrs = filter(lambda el: (ddm[0].split('_')[0] in el['name'] and ddm[1] in el['se']+el['endpoint']), ddm_json.values())[0]
            if ddm[0] not in ddm_json: ## FAKE endpoint => skip
                continue
            if 'CEPH' in ddm[0]:
                continue
            ddm_attrs = ddm_json[ddm[0]]
            for activity in filter(lambda prm: prm in ('read_wan', 'write_wan', 'delete_wan'), ddm_attrs['aprotocols'].keys()):
                cpls = [(arr[2],arr[0]) for arr in ddm_attrs['aprotocols'][activity]]
                for cpl in cpls:
                    if 'bnl' in cpl[0]:
                        path = (cpl[1]+cpl[0].split('rucio/')[0],cpl[1]+(cpl[0][0:cpl[0].rfind('disk/')+5]))['T0D1' not in cpl[0] and 'LOCAL' not in cpl[0] ]
                    elif 'eos' in cpl[0] or 'anl'in cpl[1] or 'gsiftp' in cpl[1]:
                        path = (cpl[1]+cpl[0].split('rucio')[0],cpl[1]+(cpl[0][0:cpl[0].rfind('disk/')+5]))['sgroup' in cpl[0] ]
                    else:
                        path = ((cpl[1]+cpl[0],cpl[1]+cpl[0]+'/')[cpl[0][-1] != '/'],cpl[1]+(cpl[0][0:cpl[0].rfind('disk/')+5]))['disk' not in cpl[0][-5:] ]
                        if 'pnf' in path[-3:]:
                            path = cpl[1]+cpl[0].split('perf')[0]+'atlasgroupdisk/'

                    if 'atlasuserdisk' in path:
                        continue
                    if 'DATA' in ddm[2] and ddm[2].lower().split('atlas')[1] not in path.lower() and 'BNL' not in path:
                        continue
                    if path in perm_list:
                        continue
                    perm_list.append(path)

            for p in perm_list:
                ptc = p.split(':')[0]
                if ptc == 'srm' and not srm_cric_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    srm_cric_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})
                if ptc == 'root' and not xrd_cric_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    xrd_cric_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})
                if ptc == 'davs' and not htp_cric_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    htp_cric_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})
                if ptc == 'gsiftp' and not gsi_cric_dict.has_key(p):
                    if ('atlasgroupdisk' in ddm[1] and len(ddm[0].split('_')) > 1):
                        tname = 'ATLAS'+(ddm[0].split('_'))[1]
                    else:
                        tname = ddm[2].split('T2')[-1]
                    gsi_cric_dict.update({p:{
                                'token':tname,
                                'criticality':(0,1)['DATADISK' in ddm[2]],
                                'rc_site':ddm[3],
                                'ATLAS_specific':{'ATLAS_site':ddm[0].split('_')[0]}
                                }})

        srmfile = open(srm_cric_file,'w')
        srmfile.write(json.dumps(srm_cric_dict, indent=4, sort_keys=True))
        srmfile.close()

        xrdfile = open(xrd_cric_file,'w')
        xrdfile.write(json.dumps(xrd_cric_dict, indent=4, sort_keys=True))
        xrdfile.close()

        htpfile = open(htp_cric_file,'w')
        htpfile.write(json.dumps(htp_cric_dict, indent=4, sort_keys=True))
        htpfile.close()

        gsifile = open(gsi_cric_file,'w')
        gsifile.write(json.dumps(gsi_cric_dict, indent=4, sort_keys=True))
        gsifile.close()

        return

getter = cacher()
getter.getCRICATLASInfo()

'''
                if len(cpls) > 1:
                    while cpls:
                        aggr = reduce(lambda x,y: ((x[0],x[1]+y[1]),(x[0],x[1]))[x[0]!=y[0]],cpls)
                        path = (prot+aggr[0],prot+(aggr[0].split('atlasgroupdisk/')[0]+'atlasgroupdisk/'))['atlasgroupdisk' in aggr[0] ]
                        perm_list.append(path)
                        cpls = filter(lambda cpl: cpl[0]!=aggr[0],cpls)
                else:
                    path = (prot+cpls[0][0],prot+(cpls[0][0].split('atlasgroupdisk/')[0]+'atlasgroupdisk/'))['atlasgroupdisk' in cpls[0][0] ]
                    perm_list.append(path)

                for ep in perm_list:
                    cric_dict[ddm[1]]['protocols'].append(ep)
'''
