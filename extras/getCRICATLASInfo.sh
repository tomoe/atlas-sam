#!/bin/bash

errorFile=/tmp/getCRICATLASInfo_error.txt

echo `date` "START" > $errorFile
if [ -e /usr/libexec/grid-monitoring/probes/org.atlas/getCRICATLASInfo.py ];
then
    python /usr/libexec/grid-monitoring/probes/org.atlas/getCRICATLASInfo.py >> $errorFile 2>&1
else
    python /afs/cern.ch/user/s/stupputi/public/ADC/atlasNagios/gymn/org.atlas/extras/getCRICATLASInfo.py >> $errorFile 2>&1
fi

if [ $? -ne 0 ];
then
  echo `date` "FINISHED" >> $errorFile
  echo "Porc, fallito"
  cat $errorFile | mail -s "Cron job DDM endpoints from CRIC failed" atlas-adc-sam-notifications@cern.ch
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_srm_storage_list_tmp.json ];
then
  echo "Generated SRM file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from CRIC failed" atlas-adc-sam-notifications@cern.ch
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_srm_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_srm_storage_list
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_root_storage_list_tmp.json ];
then
  echo "Generated XRootD file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from CRIC failed" atlas-adc-sam-notifications@cern.ch
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_root_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_root_storage_list
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_https_storage_list_tmp.json ];
then
  echo "Generated https file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from CRIC failed" atlas-adc-sam-notifications@cern.ch
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_https_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_https_storage_list
fi

if [ ! -s /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_gsiftp_storage_list_tmp.json ];
then
  echo "Generated gsiftp file is empty. Check error file at $errorFile. If no error reported, most likely the topology file is already younger than 7h (refreshed by hand?)" | mail -s "Cron job DDM endpoints from CRIC failed" atlas-adc-sam-notifications@cern.ch
else
  mv /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_gsiftp_storage_list_tmp.json /usr/libexec/grid-monitoring/probes/org.atlas/DDM/org.atlas/src/CRIC_gsiftp_storage_list
fi

