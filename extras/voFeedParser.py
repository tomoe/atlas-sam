#!/usr/bin/env python

import os
import sys
import datetime
from socket import gethostname
from lxml import etree
import requests

vof_url = 'https://atlas-cric.cern.ch/api/atlas/vofeed/?xml&service_state=ACTIVE&service_status=production&service_is_monitored=1'
cert    = "/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem";
key     = "/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem";

request = requests.get(vof_url, verify=False, timeout=120, cert=(cert, key))
if request.status_code != 200:
    print ("Failed to retrieve the feed at %s" % vof_url)
    sys.exit()
xdoc = etree.fromstring(request.content)

ofile = ('filtered_VOFeed.txt','/var/www/html/filtered_VOFeed.txt')['etf' in gethostname()]
nfile = ('filtered_VOFeed.txt','/var/www/html/filtered_VOFeed_tmp.txt')['etf' in gethostname()]

old_lines = sum(1 for line in open(ofile))

wdoc = open(nfile,'w')

for el in xdoc.iter():
    if el.tag == 'service' and el.attrib['flavour'] != 'PerfSonar':
        wdoc.write(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'\t'+el.attrib['flavour']+' '+el.attrib['hostname']+'\tproduction\tgreen\t'+vof_url+'\n')

wdoc.close()

new_lines = sum(1 for line in open(nfile))

if(float(new_lines)/float(old_lines) <  0.85):
    print "New file is more than 15% smaller than previous one: please check no issue has occurred. File not created."
    os.remove(nfile)
